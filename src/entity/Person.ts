import { Address } from "./Address";


export class Person {
    id:number;
    name:string;

    addresses:Address[] = [];
}