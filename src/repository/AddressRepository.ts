import { createPool, RowDataPacket } from "mysql2/promise";
import { Address } from "../entity/Address";
import { Person } from "../entity/Person";
import { PersonRepository } from "./PersonRepository";

const connection = createPool({ uri: "..." });


export class AddressRepository {

    /**
     * Exemple de méthode où on va chercher les addresses en se basant sur la 
     * clef étrangère, donc récupérer les addresses d'une personne donnée.
     * @param idPerson l'id de la personne dont on veut récupérer les addresses
     * @param withPerson Est-ce qu'on veut récupérer également la Person (Defaut: non)
     * @returns La liste des addresses de la personne ciblée
     */
    static async findByPerson(idPerson: number, withPerson = false) {
        const [rows] = await connection.query<RowDataPacket[]>("SELECT * FROM address WHERE id_person=?", [idPerson]);
        const addresses: Address[] = [];
        for (const row of rows) {
            let addr = new Address();
            addr.street = row['street'];
            if (withPerson) {
                addr.person = await PersonRepository.findById(row['id_person']);
            }
            addresses.push(addr);
        }
        return addresses;
    }
}