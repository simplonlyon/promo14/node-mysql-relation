import { createPool, RowDataPacket } from "mysql2/promise";
import { Address } from "../entity/Address";
import { Person } from "../entity/Person";
import { AddressRepository } from "./AddressRepository";

const connection = createPool({ uri: "..." });


export class PersonRepository {

    /**
     * Exemple de requête findById avec relation faite par un autre repository
     * @param id l'id de la personne à récupérer
     * @param withAddresses Est-ce qu'on veut récupérer ses addresses en même temps (Default: non)
     * @returns La Person qui contient éventuellement ses addresses
     */
    static async findById(id:number, withAddresses = false): Promise<Person> {
        const [rows] = await connection.query<RowDataPacket[]>("SELECT * FROM person");
        
        if(rows.length == 1){
            let person = new Person();
            person.id = rows[0]['id'];
            person.name = rows[0]['name'];
            //Si on va la chercher avec ses addresses
            if (withAddresses) {
                //Alors on fait un appel à un findByPerson du repository Address
                person.addresses = await AddressRepository.findByPerson(person.id);
            }
            return person;
        }
        return null;
    }
    /**
     * Exemple de findAll avec une relation OneToMany où pour chaque personne trouvée
     * on va faire un appel vers le repository de la relation (ici address) pour aller
     * chercher les entités liées à chaque personne
     * @param withAddresses Est-ce qu'on veut récupérer ses addresses en même temps (Default: non)
     * @returns Les Person qui contiennent éventuellement leurs addresses
     */
    static async findAll(withAddresses = false) {
        const [rows] = await connection.query<RowDataPacket[]>("SELECT * FROM person");
        const persons: Person[] = [];
        for (const row of rows) {
            let person = new Person();
            person.id = row['id'];
            person.name = row['name'];

            if (withAddresses) {
                person.addresses = await AddressRepository.findByPerson(person.id);
            }
            
            persons.push(person);
        }
        return persons;
    }
    /**
     * Si on cherche la performance, il serait préférable de faire des requêtes avec 
     * jointure, mais c'est pas mal plus complexe à gérer, d'autant plus si on veut
     * faire en sorte de pouvoir faire ou non la jointure selon le contexte.
     * @returns La liste des personnes et leurs addresses
     */
    static async findAllWithJoin() {
        const [rows] = await connection.query<RowDataPacket[]>("SELECT *, address.id AS id_address FROM person LEFT JOIN  address ON person.id=address.person_id");
        const persons: Person[] = [];

        for (const row of rows) {
            let person = persons.find((item => item.id === row.id));
            if (!person) {
                person = new Person();
                person.name = row['name'];
                person.id = row['id'];
            }
            let address = new Address();
            address.street = row['street'];
            address.id = row['id_address'];
            person.addresses.push(address);
        }
        return persons;
    }

  

}